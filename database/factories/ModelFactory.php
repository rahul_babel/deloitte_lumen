<?php
use Illuminate\Support\Facades\Hash; //load this to use the Hash::make method
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'name'		=> $faker->name,
        'email'		=> $faker->email,
        'phone'		=> $faker->phoneNumber,
        'address'	=> $faker->address,
        'status'	=> false,
        'password'	=> Hash::make($faker->password(6, 20))
    ];
});

$factory->define(App\Brand::class, function ($faker) {
    return [
        'name'		=> $faker->company,
    ];
});


$factory->define(App\Product::class, function ($faker) {
    
    return [
        'name'          => $faker->word,
        'description'   => $faker->sentence(),
        'brand_id'      => App\Brand::all()->random()->id
    ];
});


$factory->define(App\Serial::class, function ($faker) {
    
    return [
        'number'            => $faker->isbn13,
        'invoice_number'    => $faker->unixTime(),
        'product_id'        => App\Product::all()->random()->id
    ];
});