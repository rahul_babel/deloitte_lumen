<?php

use App\Serial;
use Illuminate\Database\Seeder;

class SerialTableSeeder extends Seeder
{
    public function run()
    {
        factory(Serial::class, 100)->create();

    }
}