<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Serial extends Model 
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'location', 'user_id', 'number', 'invoice_number', 'product_id', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = [
    //     'created_at'
    // ];

    public function product()
    {   
        return $this->belongsTo('App\Product');
    }
}
