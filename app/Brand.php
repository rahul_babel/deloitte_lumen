<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model 
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = [
    //     'created_at'
    // ];

    public function product()
    {
        return $this->hasMany('App\Product');
    }
}
