<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request; //loads the Request class for retrieving inputs
use Illuminate\Support\Facades\Hash; //load this to use the Hash::make method
use Auth;
use App;
use Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class BrandController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', [
            'only' => [
                'index', 
            ]
        ]);
    }

    public function index(Request $request)
    {
        if(Auth::user()) {
            $brands = Brand::all();
            if (null !== $request->input('hack_api_key') && $request->input('hack_api_key') == 'deloitte@123') {
                return response()->json([
                        'code' => 1,
                        'message' => 'Success',
                        'brands' => $brands
                    ], 200);
            } else {
                return response( Crypt::encrypt(
                    json_encode([
                        'code' => 1,
                        'message' => 'Success',
                        'brands' => $brands
                    ])
                ), 200);
            }
        } else {
            if (null !== $request->input('hack_api_key') && $request->input('hack_api_key') == 'deloitte@123') {
                return response()->json([
                        'code' => 0,
                        'message' => 'Fail',
                        'brands' => [
                            [
                                'id' => '',
                                'name' => '',
                                'created_at' => '',
                                'updated_at' => ''
                            ]
                        ]
                    ], 401);
            } else {
                return response( Crypt::encrypt(
                    json_encode([
                        'code' => 0,
                        'message' => 'Fail',
                        'brands' => [
                            [
                                'id' => '',
                                'name' => '',
                                'created_at' => '',
                                'updated_at' => ''
                            ]
                        ]
                    ])
                ), 401);
            }
            
        }
    }
}