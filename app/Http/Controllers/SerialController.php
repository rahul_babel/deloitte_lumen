<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Serial;
use Illuminate\Http\Request; //loads the Request class for retrieving inputs
use Illuminate\Support\Facades\Hash; //load this to use the Hash::make method
use Auth;
use App;
use Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class SerialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', [
            'only' => [
                'index', 
            ]
        ]);
    }

    public function index(Request $request)
    {
        if(Auth::user()) {
        	$errorMessage = '';
			$validator = Validator::make($request->all(), [
				'brand_id'  => 'required',
		        'serial_number' => 'required',
		        'location' => 'required'
			]);
			$errors = $validator->errors();
			foreach ($errors->all() as $message) {
				$errorMessage = $errorMessage . ' ' . $message;
			}
			if ($validator->fails()) {
				if (null !== $request->input('hack_api_key') && $request->input('hack_api_key') == 'deloitte@123') {
					return response()->json([
							'code' => 0,
							'message' => $errorMessage
						], 401);
				} else {
					return response( Crypt::encrypt(
						json_encode([
							'code' => 0,
							'message' => $errorMessage
						])
		    		), 401);
				}
			}
            $serial = Serial::where('number', $request->input('serial_number'))->first();
            if ($serial != null) {
            	if ($serial->product->brand['id'] == $request->input('brand_id')) {
            		if ($serial['user_id'] == null) {
            			$serial->user_id = Auth::user()->id;
            			$serial->location = $request->input('location');
            			$serial->save();
            			$responseMade = true;
            		} else {
            			$errorMessage = "Serial Number already used.";
            			$responseMade = false;
            		}
            	} else {
            		$errorMessage = "Brand does not match.";
            		$responseMade = false;
            	}
            } else {
            	$errorMessage = "Invalid Serial Number.";
            	$responseMade = false;
            }
            if ($responseMade) {
            	if (null !== $request->input('hack_api_key') && $request->input('hack_api_key') == 'deloitte@123') {
	                return response()->json([
	                        'code' => 1,
	                        'message' => 'Success'
	                    ], 200);
	            } else {
	                return response( Crypt::encrypt(
	                    json_encode([
	                        'code' => 1,
	                        'message' => 'Success'
	                    ])
	                ), 200);
	            }
            } else {
            	if (null !== $request->input('hack_api_key') && $request->input('hack_api_key') == 'deloitte@123') {
	                return response()->json([
	                        'code' => 0,
	                        'message' => $errorMessage
	                    ], 401);
	            } else {
	                return response( Crypt::encrypt(
	                    json_encode([
	                        'code' => 0,
	                        'message' => $errorMessage
	                    ])
	                ), 401);
            	}
            }            
        } else {
        	$errorMessage = 'Unauthorized User';
        	if (null !== $request->input('hack_api_key') && $request->input('hack_api_key') == 'deloitte@123') {
	                return response()->json([
	                        'code' => 0,
	                        'message' => $errorMessage
	                    ], 401);
	            } else {
	                return response( Crypt::encrypt(
	                    json_encode([
	                        'code' => 0,
	                        'message' => $errorMessage
	                    ])
	                ), 401);
            	}
        }
    }
}