<?php 
namespace App\Http\Controllers;

use App\User; //loads the User model
use Illuminate\Http\Request; //loads the Request class for retrieving inputs
use Illuminate\Support\Facades\Hash; //load this to use the Hash::make method
use Auth;
use App;
use Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class UserController extends Controller
{
	// public function __construct() {
	//  $this->middleware('auth:api');
	// }

	public function __construct()
    {
        $this->middleware('auth', [
        	'only' => [
        		'index', 
        		'update', 
        		'destroy'
        	]
        ]);
    }

	public function authenticate(Request $request)
	{
		$errorMessage = '';
		$validator = Validator::make($request->all(), [
			'phone' => 'required',
			'password' => 'required'
		]);
		$errors = $validator->errors();
		foreach ($errors->all() as $message) {
			$errorMessage = $errorMessage . ' ' . $message;
		}
		if ($validator->fails()) {
			return response(Crypt::encrypt(
				json_encode([
					'code' => 0,
					'message' => $errorMessage,
					'api_token' => '',
					'user' => [
		    			'id' => '',
		    			'name' => '',
		    			'email' => '',
		    			'phone' => '',
		    			'address' => '',
		    			'created_at' => '',
		    			'updated_at' => ''
		    		]
				])
    		), 401);
		}
		
		$user = User::where('phone', $request->input('phone'))->first();

		if($user != null && Hash::check($request->input('password'), $user->password)){

			$apiToken = base64_encode(str_random(40));

			User::where('phone', $request->input('phone'))->update(['api_token' => "$apiToken"]);

			// $request = Request::create('/api/read-users', 'GET');
			// $request->headers->set('Authorization', $apiToken);
			// $response = app()->dispatch($request);

			// return response()->json([
			// 	'code' => 1,
			// 	'message' => 'Successfully Logged In',
			// 	'api_token' => $apiToken,
			// 	'user' => $user
			// 	// 'user' => json_decode($response->getContent(), true)
			// 	], 200);

			return response(Crypt::encrypt(
				json_encode([
				'code' => 1,
				'message' => 'Successfully Logged In',
				'api_token' => $apiToken,
				'user' => $user
				// 'user' => json_decode($response->getContent(), true)
				])
			), 200);

		} else{
			return response(Crypt::encrypt(
				json_encode([
					'code' => 0,
					'message' => 'Invalid Credentials',
					'api_token' => '',
					'user' => [
		    			'id' => '',
		    			'name' => '',
		    			'email' => '',
		    			'phone' => '',
		    			'address' => '',
		    			'created_at' => '',
		    			'updated_at' => ''
		    		]
				])
			), 401);
		}

	}

    // public function index(Request $request) {
    // 	if ($request->input('authenticationFailed')) {
    // 		return [
    // 			'id' => '',
    // 			'name' => '',
    // 			'email' => '',
    // 			'phone' => '',
    // 			'address' => '',
    // 			'created_at' => '',
    // 			'updated_at' => ''
    // 		];
    // 	} else {
    // 		return Auth::user();
    // 	}
    // }

    // public function show($id) {
    // 	return User::findOrFail($id); 
    // }

    public function store(Request $request) {

    	$errorMessage = '';
		$validator = Validator::make($request->all(), [
			'email'  => 'required|unique:users',
	        'password' => 'required',
	        'name'  => 'required',
	        'address' => 'sometimes',
	        'phone'  => 'required|unique:users'
		]);
		$errors = $validator->errors();
		foreach ($errors->all() as $message) {
			$errorMessage = $errorMessage . ' ' . $message;
		}
		if ($validator->fails()) {
			if (null !== $request->input('hack_api_key') && $request->input('hack_api_key') == 'deloitte@123') {
				return response()->json([
						'code' => 0,
						'message' => $errorMessage
					], 401);
			} else {
				return response( Crypt::encrypt(
					json_encode([
						'code' => 0,
						'message' => $errorMessage
					])
	    		), 401);
			}
		}
 
	    $user   = new User;
	    $user->email  = $request->input('email');
	    $user->password  = Hash::make( $request->input('password') );
	    $user->name  = $request->input('name');
	    $user->address  = $request->input('address');
	    $user->phone  = $request->input('phone');
	    $user->save();

	    return response(Crypt::encrypt(
				json_encode([
					'code' => 1,
					'message' => 'Successfully Signed Up'
				])
    		));
    }

    public function update() {
    	$this->validate($request, [
	        'email'  => 'required',
	        'password' => 'sometimes',
	        'name'  => 'required',
	        'address' => 'required',
	        'phone'  => 'required'
	    ]); 
	    $user    = User::find($id);
	    $user->email   = $request->input('email');
	    if($request->has('password')){
	        $user->password = Hash::make( $request->input('password') );
	    }
	    $user->name   = $request->input('name');
	    $user->address   = $request->input('address');
	    $user->phone   = $request->input('phone');
	    $user->save();
    }

    public function destroy() {
    	$this->validate($request, [
	        'id' => 'required|exists:users'
	    ]);
	    $user = User::find($request->input('id'));
	    $user->delete();
    }
}