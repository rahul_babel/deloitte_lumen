<?php

namespace App\Http;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return view('index');
});

$app->group(['prefix' => 'api/', 'namespace' => 'App\Http\Controllers'], function ($app) {
    $app->post('create-user', 'UserController@store');
    $app->post('login-user', 'UserController@authenticate');
    $app->post('edit-user/{id}', 'UserController@update');
    $app->post('delete-user/{id}', 'UserController@destroy');
    $app->get('read-brands', 'BrandController@index');
    $app->post('check-serial', 'SerialController@index');
    // $app->get('read-users',        'UserController@index');
    // $app->get('read-user/{id}',    'UserController@show');
});