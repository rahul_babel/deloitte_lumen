<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model 
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'brand_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = [
    //     'created_at'
    // ];

    public function brand()
    {   
        return $this->belongsTo('App\Brand');
    }

    public function serial()
    {
        return $this->hasMany('App\Serial');
    }
}
